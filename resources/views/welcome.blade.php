<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bingo</title>
    <link href="{{asset('tailwind.css')}}" rel="stylesheet" type="text/css">
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
</head>
<body>
    <div class="p-4 container" id="bingo">
        <h1 class="text-center">Bingo</h1>
        <div v-if="ganador">Cartilla ganadora N°: <span v-text="ganador + 1"></span></div>
        <div class="container">
            <div class="grid grid-cols-2 gap-x-2 mt-8">
                <button class="bg-gray-500 p-4 text-white" type="submit" @click="getCartillas()" >
                    Generar cartillas
                </button>
                <button class="bg-gray-500 p-4 text-white" type="submit" @click="getNum()" >
                    Sacar número
                </button>
            </div>
        </div>
        <div class="text-xl text-center my-8" v-show="tarjetas.length > 0">Cartillas</div>
        <div class="grid grid-cols-12 gap-12">
            <div class="col-span-4 border p-4 pt-8" v-for="elem,index in tarjetas" >
                <p class="text-center" v-text="'Cartilla N° ' + (index+1)"></p>
                <div class="grid grid-cols-5 gap-4 mt-2"> 
                    <div v-for="num,index in elem" v-show="index <=11"> 
                        <p :class="getLet(index) ? 'flex items-center justify-center h-8 w-8' : ''" v-text="getLet(index)"></p>
                        <p :class="getInclude(num)" v-text="num"></p>
                    </div>
                    <div >
                        Free
                    </div>
                    <div v-for="num,index in elem"  v-show="index >11"> 
                        <p :class="getLet(index) ? 'flex items-center justify-center h-8 w-8' : ''" v-text="getLet(index)"></p>
                        <p :class="getInclude(num)" v-text="num"></p>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</body>
</html>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script>
    
new Vue({
    el: '#bingo',
     
    data:  ()=>{
        return {
            host: window.location.hostname,
            tarjetas: [],
            nums: [],
            ganador: null
        }
    },
    methods: {
    getLet(index){ 
        let letra = ''
        switch (parseInt(index,10)) {
                case 0:
                letra ='B';
                break;

                case 1:
                letra ='I';
                break;

                case 2:
                letra ='N';
                break;

                case 3:
                letra ='G';
                break;

                case 4:
                letra ='O';
                break;
            default:
                break;
        }

        return letra;
    },

    arrayEquals(a, b) { 
        let state = true
        for (let index = 0; index < a.length; index++) {  
             if (!b.includes(a[index])) {
                 return false
             }
        }
 
       
        return true; 
   },
   async getNum(){ 
    var vm = this 
    let data = await axios.get(`getNum`)
    if(!vm.nums.includes(data.data.data)){
        vm.nums.push(data.data.data) 
        vm.tarjetas.forEach((element, index) => {  
        if ( vm.arrayEquals( element, vm.nums)){ 
            vm.ganador = index
        }
    });
    }
   
    
   
   },

   async getCartillas(){
    var vm = this  
    let data = await axios.get(`getCartillas/8`)
    
    vm.tarjetas = data.data.data; 
     
    },

    getInclude(num){
        var vm = this 
        let state = vm.nums.includes(num)
        return state ? 'p-3 bg-green-600 rounded-full flex items-center justify-center h-8 w-8 text-white' : 'p-3 rounded-full flex items-center justify-center h-8 w-8'
         
    }
 }
});


</script>