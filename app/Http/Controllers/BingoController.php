<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;

class BingoController extends Controller
{
    public function getCartillas($count)
    {
        try {
         
            $result = [];
            $arr_b = [];
            $arr_i= [];
            $arr_n= [];
            $arr_g= [];
            $arr_o= [];
            $arr_formated = [];
            $arr_formated_session= [];
            for ($index = 0; $index < intval($count); $index++) {
            
                    while(count($arr_b) < 6){
                        $r = floor(random_int(1, 15) ) ; 
                        if( !array_search($r, $arr_b)  ) {
                            array_push($arr_b, $r);
                            array_push($arr_formated_session, $r); 
                        } 
                    }

                    while(count($arr_i) < 6){
                        $r = floor(random_int(16, 30) ) ; 
                        if( !array_search($r, $arr_i)  ) {
                            array_push($arr_i, $r);
                            array_push($arr_formated_session, $r);
                        } 
                    }

                    while(count($arr_n) < 6){
                        $r = floor(random_int(31, 45) ) ; 
                        if( !array_search($r, $arr_n)  ) {
                            array_push($arr_n, $r);
                            array_push($arr_formated_session, $r);
                        } 
                    }

                    while(count($arr_g) < 6){
                        $r = floor(random_int(46, 60) ) ; 
                        if( !array_search($r, $arr_g)  ) {
                            array_push($arr_g, $r);
                            array_push($arr_formated_session, $r);
                        } 
                    }

                    while(count($arr_o) < 6){
                        $r = floor(random_int(61, 75) ) ; 
                        if( !array_search($r, $arr_o)  ) {
                            array_push($arr_o, $r);
                            array_push($arr_formated_session, $r);
                        } 
                    }

                   
                   
                   
                    for ($i=0; $i < 5; $i++) {  
                        array_push($arr_formated, $arr_b[$i]) ;
                        array_push($arr_formated, $arr_i[$i]) ;
                        if ($i !== 2) {
                            array_push($arr_formated, $arr_n[$i]) ;
                        }
                        
                        array_push($arr_formated, $arr_g[$i]) ;
                        array_push($arr_formated, $arr_o[$i]) ;
                    }
                    
                 array_push($result, $arr_formated) ;
                 $arr_b = [];
                 $arr_i = [];
                 $arr_n = [];
                 $arr_g = [];
                 $arr_o = [];

                 $arr_formated = [];
                
            }
           
            $result_data = array(
                'data'=> $result
            );
             Session::put('cartillas', $arr_formated_session);
            return json_encode($result_data) ;
        } catch (\Throwable $th) {
          throw $th;
        }
    }

    public function getNum(Request $request){
        try { 
                $num = random_int(0,75);
              
                while(  !array_search($num, Session::get('cartillas'))  ){
                   $num = random_int(0,75);
                } 
                $result_data = array(
                    'data'=> $num
                );
                return json_encode($result_data) ;
        } catch (\Throwable $th) {
            throw $th;
        }
         
    }
}
